import Dependencies._


lazy val root = (project in file("."))
  .aggregate(bib_edit, core, helper, renamer, cli, parser_test)

lazy val helper = project.settings(
  commonSettings,
).dependsOn(bib_edit, renamer)

lazy val bib_edit =
  project.settings(commonSettings, 
    name := "bib_edit",

      mainClass in(Compile, run) := Some("de.ironjan.bib_helper.bib_edit.BibEdit"))
    .dependsOn(core)

lazy val renamer =
  project.settings(commonSettings,
    name := "renamer",
    mainClass in(Compile, run) := Some("de.ironjan.bib_helper.renamer.Renamer"))
    .dependsOn(core)

lazy val core =
  project.settings(commonSettings)

lazy val parser_test =
  project.settings(commonSettings,
    name := "parser_test",
    mainClass in(Compile, run) := Some("Main"))
    .dependsOn(core)

lazy val cli =
  project.settings(commonSettings,
    name := "cli",
    mainClass in (Compile, run):= Some("de.ironjan.bib_helper.cli.CLI"))
  .dependsOn(core, check)

lazy val check =
  project.settings(commonSettings,
    name:="check"
  ).dependsOn(core)

val commonSettings = inThisBuild(List(
  organization := "de.ironjan",
  scalaVersion := "2.12.5",
  version := "0.1.0-SNAPSHOT",
  libraryDependencies ++= commonDependencies
))



lazy val commonDependencies =
  Seq("org.parboiled" %% "parboiled" % "2.1.4",
    "com.github.dwickern" %% "scala-nameof" % "1.0.3" % "provided",
      "org.backuity.clist" %% "clist-core"   % "3.4.0",
      "org.backuity.clist" %% "clist-macros" % "3.4.0" % "provided",
    scalaTest % Test)
