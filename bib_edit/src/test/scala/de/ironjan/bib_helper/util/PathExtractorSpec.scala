package de.ironjan.bib_helper.util

import de.ironjan.bib_helper.TestResources
import org.scalatest._

class PathExtractorSpec
extends FlatSpec
with Matchers
  with TestResources{

  private object TestImplementation extends PathExtractor {}

  "extractFilePath" should "throw InvalidArgumentException when given empty argument list" in {
    intercept[IllegalArgumentException] {
      TestImplementation.extractPath(Array.empty[String])
    }
  }

  it should "return the file name when given an argument list and no index" in {
    TestImplementation.extractPath(Array("/some/path")) shouldBe "/some/path"
  }


  it should "return the file name when given an argument list and an index" in {
    TestImplementation.extractPath(Array("asd", "/some/path"), 1) shouldBe "/some/path"
  }


}