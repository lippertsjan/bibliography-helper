package de.ironjan.bib_helper.bib_edit

import java.io.{File, FilenameFilter}

import de.ironjan.bib_helper.bib_tex_lib._
import de.ironjan.bib_helper.conf.Configuration
import de.ironjan.bib_helper.util.{Module, PathExtractor}
import de.ironjan.bib_helper.bib_tex_lib.PrettyPrinter.BibTexDataPrettyPrinter
import org.backuity.clist._

import scala.util.{Failure, Success, Try}

object BibEdit
  extends App
    with PathExtractor
    with UserInput {

    val filePath = extractPath(args)

    val file = new File(filePath)

    val files = if (file.isDirectory) file.listFiles(new FilenameFilter {
      override def accept(dir: File, name: String): Boolean = name.endsWith(".bib")
    }) else Array(file)


    BibTexParser.load(files.head) match {
      case Success(bibTexFile) => {
        println("Press Ctrl+C to exit.")
        printUsageInfo
        editorLoop(bibTexFile)
      }
      case Failure(e) => {
        println(s"Skipping the file ${files.head}: $e \n...")
      }
    }

    files.tail
      .flatMap { file =>
        BibTexParser.load(file) match {
          case Success(bibTexFile) => {
            println("Press Ctrl+C to exit.")
            printUsageInfo
            editorLoop(bibTexFile)
            None
          }
          case Failure(e) => {
            println(s"Skipping file $file: $e")
            Some(file)
          }
        }
      }.foreach(f => println(s"Skipped ${f.getAbsolutePath}"))

  def printUsageInfo = {
    println(
      """|Commands:
         |  a <key> <value> - adds a new key value pair. Everything after the second space is part of the value
         |  u <key> <value> - updates a key value pair. Everything after the second space is part of the value
         |  r <key> - remove key value pair
         |  t <newType> - changes ref type to newType
         |  q - update quote key
         |  v - view and validate the contents
         |  e - exit without saving
         |  s - save and open next file or exit
      """.stripMargin)
  }

  private def addCommand(cmd: String, bibTexData: BibTexData) = {
    // TODO ignore existing keys!
    val (key, value) = extractKeyValFromCommand(cmd)

    val newPairs = bibTexData.kvPairs :+ KeyValuePair(key, value)
    bibTexData.copy(kvPairs = newPairs)
  }

  private def updateCommand(cmd: String, bibTexData: BibTexData): BibTexData = {
    val (key, value) = extractKeyValFromCommand(cmd)

    val newPairs = bibTexData.kvPairs.filterNot(_.key == key) :+ KeyValuePair(key, value)
    bibTexData.copy(kvPairs = newPairs)
  }

  def removeCommand(cmd: String, bibTexData: BibTexData): BibTexData = {
    val (key, value) = extractKeyValFromCommand(cmd)

    val newPairs = bibTexData.kvPairs.filterNot(_.key == key)
    bibTexData.copy(kvPairs = newPairs)
  }

  def updateTypeCommand(cmd: String, bibTexData: BibTexData): BibTexData = {
    val newType = extractValFromCommand(cmd)

    newType match {
      case Success(value) => bibTexData.copy(refType = value)
      case Failure(_) => {
        println("Type needs parameter")
        bibTexData
      }
    }
  }

  def updateQuoteKeyCommand(cmd: String, edittedData: BibTexData): BibTexData = {
    val input = extractValFromCommand(cmd)

    val value =
      if (input.isSuccess && input.get.nonEmpty)
        input.get
      else {
        val author = edittedData.author.getOrElse(" ")
        val year = edittedData.year.getOrElse(" ")
        s"$author.$year"
      }

    if (value.contains(" ")) println(s"Invalid value: $value; all spaces will be replaced with dashes")
    edittedData.copy(quoteKey = value.replaceAll(" ", "-"))
  }

  def extractValFromCommand(cmd: String) = Try(extractKeyValFromCommand(cmd)._1)

  private def extractKeyValFromCommand(cmd: String) = {
    val args =
      cmd.split(" ")
        .tail


    val key = args.head
    val value = args.tail.mkString(" ")
    (key, value)
  }

  def saveCommand(bibTexFile: BibTexFile) = {
    BibTexWriter.save(bibTexFile) match {
      case Success(_) => {
        println("File saved successfully.")
      }
      case Failure(e) => {
        println("Could not save:")
        e.printStackTrace()
      }
    }
  }


  def viewAndValidate(edittedData: BibTexData) = {
    println(edittedData.pp)
    println()

    edittedData.validate match {
      case Success(_) => println("Valid.")
      case Failure(exception) => exception.getMessage
    }
  }

  def editorLoop(bibTexFile: BibTexFile, firstLoop: Boolean = true): Unit = {
    var edittedData = bibTexFile.data
    viewAndValidate(edittedData)

    var hasSaved = false

    var input = readUserInput
    while (input != null) {
      input match {
        case cmd if cmd.startsWith("a") => edittedData = addCommand(cmd, edittedData)
        case cmd if cmd.startsWith("u") => edittedData = updateCommand(cmd, edittedData)
        case cmd if cmd.startsWith("r") => edittedData = removeCommand(cmd, edittedData)
        case cmd if cmd.startsWith("t") => edittedData = updateTypeCommand(cmd, edittedData)
        case cmd if cmd.startsWith("q") => edittedData = updateQuoteKeyCommand(cmd, edittedData)
        case cmd if cmd.startsWith("v") => viewAndValidate(edittedData)
        case cmd if cmd.startsWith("e") => System.exit(0)
        case cmd if cmd.startsWith("s") => {
          saveCommand(bibTexFile.copy(data = edittedData))
          viewAndValidate(edittedData)
          hasSaved = true
        }
        case _ => {
          println("Unknown command.")
          printUsageInfo
          viewAndValidate(edittedData)
        }
      }

      input = if (hasSaved) null else readUserInput
    }
  }
}