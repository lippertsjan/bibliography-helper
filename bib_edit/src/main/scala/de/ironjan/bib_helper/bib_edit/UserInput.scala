package de.ironjan.bib_helper.bib_edit

trait UserInput {
  def readUserInput = io.StdIn.readLine("> ")
}
