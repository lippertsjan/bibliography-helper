package de.ironjan.bib_helper

import de.ironjan.bib_helper.bib_edit.BibEdit
import de.ironjan.bib_helper.conf.DefaultConfiguration
import de.ironjan.bib_helper.renamer.Renamer

object BibliographyHelper
extends App 
with AppUsageInformation {

  if(args.size == 0) {
    println(UsageInfo)
    System.exit(0)
  }

  try {
    val conf = DefaultConfiguration
    val module = args(0) match {
      case s if s.startsWith("r")
      => println("Use standalone jar!")
      case s if s.startsWith("b")
      => println("Use standalone jar!")
      case _ => {
        println(UsageInfo)
        System.exit(1)
      }
    }
    } catch {
      case e: Exception => {
        println(UsageInfo)
        println("\nUncaught exception:")
        e.printStackTrace
        System.exit(1)
      }
    }

}


