package de.ironjan.bib_helper

trait TestResourceFiles {
  val resourcePath = "./src/test/resources/"
  val hars01Path = resourcePath + "hars01.pdf.bib"
}
