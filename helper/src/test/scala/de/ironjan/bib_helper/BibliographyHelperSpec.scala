package de.ironjan

import org.scalatest._

class BibliographyHelperSpec
  extends FlatSpec
    with Matchers {
  "1+1" should " be 2" in {
    1 + 1 should be (2)
  }
}
