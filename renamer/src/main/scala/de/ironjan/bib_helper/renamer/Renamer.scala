package de.ironjan.bib_helper.renamer

import java.io.File

import de.ironjan.bib_helper.bib_tex_lib.BibTexParser
import de.ironjan.bib_helper.renamer.InputFolderHandling.InputFolderFileExt

object Renamer
  extends App {

  if (args.length != 1) {
    println("Usage: renamer <dir>")
    System.exit(1)
  }

  val path = args(0)
  val folder = new File(path)
  val logFile = new File(path + "/" + ".proccessed_bibs")

  folder.unprocessedFiles(logFile)
    .foreach { bibPdf =>
      FileProcessor(folder).process(bibPdf, logFile)
    }

}

