package de.ironjan.bib_helper.renamer

import de.ironjan.bib_helper.bib_tex_lib.BibTexFile

case class FilePair(bibTexFile: BibTexFile,
                    pdfPath: String)