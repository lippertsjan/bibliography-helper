package de.ironjan.bib_helper.renamer

import java.io.{File, FileFilter}

import de.ironjan.bib_helper.bib_tex_lib.BibTexParser

class InputFolder(path: String)
  extends File(path: String) {

  def bibFiles: Set[String] = {
    findFilesByExtensions(".pdf.bib")
      .map(_.getAbsolutePath)
      .toSet
  }


  def validBibFiles: Set[String] = bibFiles.filter(BibTexParser.load(_).isSuccess)

  private def findFilesByExtensions(extension: String) = {
    listFiles(new FileFilter {
      override def accept(pathname: File): Boolean = pathname.getName.endsWith(extension)
    })
  }

  def withCompanionPdfs(bibFiles: Set[String]): Set[(String, String)] = {
    val pdfFiles = findFilesByExtensions(".pdf")

    pdfFiles.filter{f =>
      bibFiles.contains(f.getAbsolutePath + ".bib")
    }.map{f =>
      (f.getAbsolutePath, f.getAbsolutePath + ".bib")
    }.toSet
  }

  def processableFilePairs = {
    withCompanionPdfs(validBibFiles).map{case (b,p) =>
      BibTexParser.load(b)
        .map { bt =>
          FilePair(bt, p)
        }
    }.flatMap(_.toOption)
  }
}
