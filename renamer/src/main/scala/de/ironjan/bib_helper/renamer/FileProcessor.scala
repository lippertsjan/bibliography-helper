package de.ironjan.bib_helper.renamer

import java.io.{File, FileWriter}

import de.ironjan.bib_helper.bib_tex_lib.{BibTexData, BibTexFile, BibTexParser}

import scala.util.Try


case class FileProcessor(outputFolder: File)

object FileProcessor {

  implicit class FileProcessorExt(fp: FileProcessor) {

    def process(bibPdf: BibPdf, logFile: File = new File("/dev/null")) {
      if (!bibPdf.isValid) {
        println(s"${bibPdf.bib} is not valid and will be skipped.")
        return
      }

      BibTexParser.load(bibPdf.bib.getAbsolutePath)
        .map(extractName) match {
        case scala.util.Success(newName) => {
          tryRename(bibPdf, newName, logFile)
        }
        case scala.util.Failure(e) => {
          println(s"Could not find name of ${bibPdf.bib} because of $e.")
        }
      }
    }

    def tryRename(bibPdf: BibPdf, name: String, logFile: File): Unit = {
      import java.nio.file.Files.copy
      val targetPathWithoutExtension = fp.outputFolder.getAbsolutePath + "/" + name

      val bib = bibPdf.bib
      val pdf = bibPdf.pdf

      val bibTargetPath = new File(targetPathWithoutExtension + ".bib").toPath
      copy(bib.toPath, bibTargetPath)


      val pdfTargetPath = new File(targetPathWithoutExtension + ".pdf").toPath
      copy(pdf.toPath, pdfTargetPath)

      val fw = new FileWriter(logFile, true)
      try {
        fw.write(f"${bib.getAbsolutePath}%n")
      }
      finally {
        fw.close()
      }
    }

    private def extractName(file: BibTexFile) = {
      val data = file.data
      val author = data.author.get
      val year = data.year.get
      val title = data.title.get

      f"${author.toLowerCase}%s$year%02d-${cleanTitle(title)}%s"
        .replaceAll("--", "-")
        .replaceAll(" \n\r\t\f", "")
    }
  }

  private def cleanTitle(title: String) =
    title.toLowerCase
      .replaceAll(" ", "_")
      .replaceAll("[^\\w\\s]", "")
      .replaceAll("_", "-")


}
