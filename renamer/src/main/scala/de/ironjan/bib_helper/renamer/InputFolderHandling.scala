package de.ironjan.bib_helper.renamer

import java.io.{File, FileFilter}

import scala.io.Source
import scala.util.{Success, Try}


object InputFolderHandling {
  implicit class InputFolderFileExt(inputFolder: File) {

    def processableFiles = 
      inputFolder.listFiles(new FileFilter {
        override def accept(pathname: File): Boolean = pathname.getName.endsWith("bib")
      })
    .toSeq
    .map { f => (f, findCompanionPdf(f)) }
    .filter(_._2.isDefined)
    .map{ case (f,c) => BibPdf(f, c.get)}
    

    private def findCompanionPdf(f: File) = {
      val path = f.getAbsolutePath
      path.substring(0, path.length - 3) + "pdf"

      val pdf = new File(path)

      if (pdf.exists()) Some(pdf) else None
    }

    def unprocessedFiles(logFile: File) = {
      val proccessed = proccessedFiles(logFile)

      processableFiles
      .map{ bibPdf =>
        (bibPdf.bib.getAbsolutePath, bibPdf)
      }.filterNot{tuple => 
        proccessed.contains(tuple._1)
      }
      .map(_._2)
  }

  def proccessedFiles(logFile: File) = {
    Try(Source.fromFile(logFile, "UTF-8")
      .getLines
      .toSet
    ) match {
      case Success(set) => set
      case _ => Set.empty[String]
      }
    }
  }
}
