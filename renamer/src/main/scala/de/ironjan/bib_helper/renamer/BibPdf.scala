package de.ironjan.bib_helper.renamer

import java.io.File

import de.ironjan.bib_helper.bib_tex_lib.BibTexParser

case class BibPdf(bib: File,
                  pdf: File) {
  def isValid = {
    val triedFile = BibTexParser.load(bib.getAbsolutePath)
    triedFile match {
      case scala.util.Success(bf) =>
        bf.data.hasMandatoryFields
      case scala.util.Failure(e) => {
        println(s"Could not load ${bib.getAbsoluteFile}: $e")
        false
      }
    }
  }
}
