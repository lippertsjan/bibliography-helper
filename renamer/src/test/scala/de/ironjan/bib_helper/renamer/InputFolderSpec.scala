package de.ironjan.bib_helper.renamer

import java.io.{File, FileFilter}

import de.ironjan.bib_helper.TestResources
import org.scalatest.{FlatSpec, Matchers}

class InputFolderSpec
  extends FlatSpec
    with Matchers
    with TestResources {



  "bibFiles" should "list none if there are none" in {
    new MockInputFolder(Array.empty[File]).bibFiles.size shouldBe 0
  }

  it should "return one file if there is one file" in {
    new MockInputFolder(Array(new File("/a.pdf.bib"))).bibFiles.size shouldBe 1
  }

  it should "return 3 files if there are 3 files" in {
    new MockInputFolder(
      Array(new File("/a.pdf.bib"),
        new File("/b.pdf.bib"),
        new File("/c.pdf.bib")))
      .bibFiles.size shouldBe 3
  }

  "validBibFiles" should "list none if there are none" in {
        new MockInputFolder(Array.empty[File]).validBibFiles.size shouldBe 0
  }

  it should "return 1 file if there is 1 valid file" in {
    new MockInputFolder(
      Array(new File(hars01Path))).validBibFiles.size shouldBe 1
  }

  it should "return 1 file if there is 1 valid file in a set of 3 files" in {
    new MockInputFolder(
      Array(new File("/a.pdf.bib"),
        new File("/b.pdf.bib"),
        new File(hars01Path))).validBibFiles.size shouldBe 1
  }

  "withCompanionPdfs" should "find no companion pdfs for an empty set" in {
    new MockInputFolder(
      Array(new File("/a.pdf.bib"),
        new File("/b.pdf.bib"),
        new File("/c.pdf.bib")))
      .withCompanionPdfs(Set.empty[String]).size shouldBe 0
  }

  it should "find 1 companion pdf if there is 1" in {
    val mockInputFolder = new MockInputFolder(
      Array(new File("/a.pdf.bib"),
        new File("/a.pdf"),
        new File("/b.pdf.bib"),
        new File("/c.pdf.bib")))
    mockInputFolder.withCompanionPdfs(mockInputFolder.bibFiles) shouldBe Set(("/a.pdf","/a.pdf.bib"))
  }

  it should "find 3 companion pdfs if there are 3" in {
    val mockInputFolder = new MockInputFolder(
      Array(new File("/a.pdf.bib"),
        new File("/a.pdf"),
        new File("/b.pdf"),
        new File("/b.pdf.bib"),
        new File("/c.pdf"),
        new File("/c.pdf.bib")))
    mockInputFolder.withCompanionPdfs(mockInputFolder.bibFiles) shouldBe
      Set(("/a.pdf","/a.pdf.bib"),
        ("/b.pdf","/b.pdf.bib"),
        ("/c.pdf","/c.pdf.bib"))
  }

  class MockInputFolder(mockListedFiles: Array[File])
    extends InputFolder("/") {
    override def listFiles(fileFilter: FileFilter): Array[File] = mockListedFiles.filter(fileFilter.accept)
  }

}
