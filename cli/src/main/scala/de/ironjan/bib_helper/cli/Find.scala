package de.ironjan.bib_helper.cli

import java.nio.file.{Files, Path, Paths}

import de.ironjan.bib_helper.bib_tex_lib.{BibTexFile, BibTexParser}
import de.ironjan.bib_helper.util.FileHelper
import org.backuity.clist.{Command, arg, args}

import scala.util.Success

object Find
  extends Command
    with Common {

  var pathAsString = arg[String]()
  var terms = args[List[String]]()

  def findInField(term: String, b: BibTexFile, key: String) = {
    val valueOrEmpty = b.data.value(key).getOrElse("")
    if (valueOrEmpty.contains(term)) {
      Some(s"$key match: $valueOrEmpty")
    } else None
  }

  def findIn(term: String, b: BibTexFile) = {
    val results = Seq(findInField(term, b, "author"),
      findInField(term, b, "title"),
      findInField(term, b, "year"),
      findInField(term, b, "abstract")).flatten

    if (results.nonEmpty)
      Some((b, results))
    else None
  }

  def getMatcher(term: String) = (p: Path) => {
    BibTexParser.load(p) match {
      case Success(b: BibTexFile) => findIn(term, b)
      case _ => {
        System.err.println(s"Could not load $p")
        None
      }
    }
  }


  override def run(): Unit = {
    var term = terms.mkString(" ")
    println(s"Searching in $pathAsString...")

    val matcher = getMatcher(term)

    val results = FileHelper.findBibFiles(Paths.get(pathAsString))
      .map(matcher)
      .flatten

    if (results.nonEmpty) {
      println(s"${results.size} results:")
      println()
      results.foreach { case (b, results) =>
        println(b.path)
        results.foreach(println)
        println()
      }
    } else {
      println("No results.")
    }
  }
}
