package de.ironjan.bib_helper.cli

import java.nio.file.Paths

import org.backuity.clist.{Command, arg}

trait PathArgument extends Command {

  var pathAsString: String = arg[String]()

  def path = Paths.get(pathAsString)
}
