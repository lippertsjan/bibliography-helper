package de.ironjan.bib_helper.cli

import java.io.{File, FilenameFilter}
import java.nio.file.{Files, Path, Paths, StandardCopyOption}

import de.ironjan.bib_helper.bib_tex_lib.BibTexParser
import de.ironjan.bib_helper.cli.Edit.{editLogic, pathAsString}
import org.backuity.clist.{Command, arg}

import scala.reflect.io.File

object  Rename
extends Command
with Common
  with PathArgument {

  override def run(): Unit = {

    if (!Files.exists(path)){
      System.err.println(s"$pathAsString does not exist.")
      System.exit(1)
    }
    run(path)
  }

  def run(path: Path): Unit = {

    val file = path.toFile


    if (file.isDirectory) {
      println(s"handle folder: $path")
      handleFolder(path)
    }
    else {
      println(s"handle file: $path")
      handleFile(path)
    }
  }

  def handleFolder(path: Path) = {
    val files = path.toFile.listFiles(new FilenameFilter {
      override def accept(dir: java.io.File, name: String): Boolean = name.endsWith(".bib")
    }).map(f => Paths.get(f.getPath))
    files.foreach(println)
    files.foreach(handleFile)
  }

  def normalize(title: Option[String]) = title.map(s =>
    s.toLowerCase
    .replaceAll("/","")
    .replaceAll("\\\\","")
    .replaceAll("\\?","")
    .replaceAll("%","")
    .replaceAll("\\*","")
    .replaceAll(":","")
    .replaceAll("\\|","")
    .replaceAll("\"","")
    .replaceAll("<","")
    .replaceAll(">","")
    .replaceAll(",","")
    // /\?%*:|"<>.
  )

  def handleFile(path: Path):Unit = {
    val pathAsString = path.toString

    if(!pathAsString.endsWith(".bib")) {
      System.out.println("Cannot rename non-bib files directly.")
      return ;
    }

    var dir = path.toAbsolutePath.getParent.toFile

    val bibFile = path.toFile
    var pdfFile = pathAsString.replace(".pdf.bib", ".pdf")

    System.out.println(s"$bibFile + $pdfFile")
    BibTexParser.load(bibFile) match {
      case scala.util.Success(bibData) => {
        val data = bibData.data

        val newNameParts = List(data.firstAuthor.toLowerCase + data.year.map(i=>i.toString).getOrElse("YY"))++: normalize(data.title).getOrElse("").split(" ")

        val newBaseName = newNameParts.mkString("-")


        val newpdfname = newBaseName + ".pdf"
        val newbibname = newpdfname + ".bib"

        val newpdfpath = new java.io.File(dir, newpdfname).getAbsolutePath
        val newbibpath = new java.io.File(dir, newbibname).getAbsolutePath

        mv(pdfFile, newpdfpath)
        mv(bibFile.getPath, newbibpath)

      }
      case _ => {
        System.err.println(s"Error loading $bibFile")
      }
    }


  }

  def mv(src:String, dst: String): Unit = {
    val result = Files.move(
      Paths.get(src),
      Paths.get(dst),
      StandardCopyOption.REPLACE_EXISTING
    )
    if(result == null){
      println(s"Failed: mv $src $dst")
    }
    if(verbose) {
      println(s"$src -> $dst")
    }
  }

  case class FileNameData(firstAuthor: String, year:Int, title:String)
}
