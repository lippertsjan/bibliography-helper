package de.ironjan.bib_helper.cli

import org.backuity.clist.{Cli, Command, opt}

object CLI extends App {
  Cli.parse(args)
    .withCommands(Check, Download, Edit, Find, Rename)
    .foreach(_.run())
}







