package de.ironjan.bib_helper.cli

import org.backuity.clist.{Command, opt}

trait Common { this: Command =>
  var verbose = opt[Boolean](abbrev = "v")
   def run(): Unit
}
