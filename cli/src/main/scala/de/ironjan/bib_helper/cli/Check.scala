package de.ironjan.bib_helper.cli

import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{FileVisitOption, Files, Path, Paths}
import java.util.function.BiPredicate
import java.util.stream.Collectors

import de.ironjan.bib_helper.check.BibChecker
import org.backuity.clist.{Command, arg}
import de.ironjan.bib_helper.util.FileHelper._

object Check
  extends Command
    with Common {

  var pathAsString = arg[String]()

  override def run(): Unit = {
    val path = Paths.get(pathAsString)

    if (!Files.isDirectory(path)) {
      System.err.println(s"$pathAsString is not a directory.")
      System.exit(1)
    }

    if (!Files.exists(path)) {
      System.err.println(s"$pathAsString does not exist.")
      System.exit(1)
    }


    run(path)
  }


  def run(path: Path): Unit = {

    val bibs = findBibFiles(path).sorted
    val pdfs = findFilesByExtension(path, ".pdf").sorted

    //    val pdfBasedPairs = pdfs.map(p => (p,bibs.filter(b=>p.toString.startsWith(b.toString))))
    //    val bibBasedPairs = bibs.map(b => (b, pdfs.filter(p=>p.toString.startsWith(b.toString))))

    val pdfsWithoutBib = pdfs.filter(p => !bibs.exists(b => b.toString.startsWith(p.toString)))
    val bibsWithoutPdf = bibs.filter(b => !pdfs.exists(p => b.toString.startsWith(p.toString)))

    println("PDFs without BIB:")
    pdfsWithoutBib.foreach(println)
    println()
    println("BIBs without PDF:")
    bibsWithoutPdf.foreach(println)
    println()
    println("Individual errors:")
    bibs.map(b => (b, BibChecker.check(b)))
      .filter(_._2.nonEmpty)
      .foreach { case (b, list) => {
        println(b.toString)
        list.foreach(println)
      }
      }
  }

}
