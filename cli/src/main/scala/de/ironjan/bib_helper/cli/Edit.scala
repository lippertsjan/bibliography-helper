package de.ironjan.bib_helper.cli

import java.io._

import de.ironjan.bib_helper.bib_tex_lib._
import org.backuity.clist._
import de.ironjan.bib_helper.bib_tex_lib.PrettyPrinter._
import org.parboiled2._

import scala.collection._
import scala.util._

import java.nio.file._

object Edit
  extends Command
    with Common
with PathArgument {


  def run(): Unit = {
    println("Start bib edit")


    if (!Files.exists(path)){
        System.err.println(s"$pathAsString does not exist.")
        System.exit(1)
      }
    editLogic(path)
  }

  def editLogic(path: Path): Unit ={
    println(s"Edit $path")


    val file = path.toFile

    val files = if (file.isDirectory) file.listFiles(new FilenameFilter {
      override def accept(dir: File, name: String): Boolean = name.endsWith(".bib")
    }) else Array(file)

    BibTexParser.load(files.head) match {
      case Success(bibTexFile) => {
        println("Press Ctrl+C to exit.")
        editorLoop(bibTexFile)
      }
      case Failure(e: ParseError) => {
        println(s"Skipping the file ${files.head}: $e \n...")
        e.printStackTrace()
        e.traces.foreach(println)
      }
      case Failure(e) => {
        println(s"Skipping the file ${files.head}: $e \n...")
        e.printStackTrace()
      }
    }

    val tuples = files.tail
      .flatMap { file =>
        BibTexParser.load(file) match {
          case Success(bibTexFile) => {
            println("Press Ctrl+C to exit.")
            editorLoop(bibTexFile)
            None
          }
          case Failure(e) => {
            Some((file, e))
          }
        }
      }
    tuples.foreach {case (f, e) => println(s"Skipped ${f.getAbsolutePath}: $e")
    }

  }
  def readUserInput: String = io.StdIn.readLine("> ")


  def editorLoop(bibTexFile: BibTexFile, firstLoop: Boolean = true): Unit = {
    var edittedData = bibTexFile.data

    var mKVP = mutable.ListBuffer[KeyValuePair]()
    edittedData.kvPairs.foreach(p => mKVP = mKVP.+:(p))
    var mutData = MutableBibTexData(edittedData.refType, edittedData.quoteKey, mKVP)

    View.run(mutData)

    var hasSaved = false

    var input = readUserInput

    while (input != null && mutData.continueEditting) {
      try {
        Cli.parse(input.split(" "))
          .withHelpCommand("help")
          .throwExceptionOnError()
          .withCommands(Add,
            Update,
            Remove,
            View,
            new Save(bibTexFile),
            new Next(bibTexFile),
            Quote,
            Defquote,
            Exit)
          .foreach(cmd => mutData=cmd.run(mutData))
      }catch {
        case _: ParsingException => {/*do nothing*/}
        case e: Throwable => {
          e.printStackTrace()
          System.exit(1)
        }
      }

      if(mutData.continueEditting){
        input = if (hasSaved) null else readUserInput
      }
    }
  }
}

trait EditCommand extends Command {
  def run(bibTexData: MutableBibTexData): MutableBibTexData
}

trait EditKeyCommand extends EditCommand {
  var key: String = org.backuity.clist.arg[String]()

}

trait EditKeyValCommand extends EditKeyCommand {
  var vals: List[String] = org.backuity.clist.args[List[String]]()

  def arg(): String = vals.mkString(" ")

}

object Add extends EditKeyValCommand {

  override def run(m: MutableBibTexData) = {
    m.kvPairs.find(_.key == key).foreach(m.kvPairs -= _)
    m.kvPairs += KeyValuePair(key, arg)
    m
  }
}


object View extends EditCommand {
  override def run(m: MutableBibTexData) = {
    println(m.pp)
    println()

    m.validate match {
      case Success(_) => println("Valid.")
      case Failure(exception) => exception.getMessage
    }
    m
  }
}

object Update extends EditKeyValCommand {
  override def run(m: MutableBibTexData) = {
    m.kvPairs.find(_.key == key).foreach(m.kvPairs -= _)
    m.kvPairs += KeyValuePair(key, arg)
    m
  }
}

object Quote extends EditKeyCommand {
  override def run(m: MutableBibTexData)={
 m.copy(quoteKey = key)
  }
}
object Defquote extends EditCommand {
  override def run(m: MutableBibTexData): MutableBibTexData = {
    m.copy(quoteKey = m.defaultQuoteKey)
  }
}
object Remove extends EditKeyCommand {
  override def run(m: MutableBibTexData) = {
    m.kvPairs -= m.kvPairs.find(_.key == key).get
    m
  }
}

object Exit extends EditCommand {
  override def run(m: MutableBibTexData) = {
    System.exit(0)
    m
  }
}

class Save(bibTexFile: BibTexFile) extends EditCommand {
  override def run(m: MutableBibTexData) = {
    BibTexWriter.save(
      bibTexFile.copy(data = BibTexData(m.refType, m.quoteKey, m.kvPairs))
    ) match {
      case Success(_) => {
        println("File saved successfully.")
      }
      case Failure(e) => {
        println("Could not save:")
        e.printStackTrace()
      }
    }

    m
  }
}
class Next(bibTexFile:BibTexFile) extends EditCommand{
  override def run(m: MutableBibTexData) = {
    new Save(bibTexFile).run(m)
    m.copy(continueEditting = false)
  }
}