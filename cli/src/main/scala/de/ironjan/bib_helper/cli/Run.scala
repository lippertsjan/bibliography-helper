package de.ironjan.bib_helper.cli

import org.backuity.clist.Command

object Run extends Command with Common {
  def run(): Unit = {
    println("Running...")
  }
}
