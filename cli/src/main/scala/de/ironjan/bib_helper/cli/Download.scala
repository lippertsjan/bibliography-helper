package de.ironjan.bib_helper.cli

import java.nio.file.{Files, Path, Paths}

import de.ironjan.bib_helper.bib_tex_lib.{BibTexFile, BibTexParser}
import de.ironjan.bib_helper.cli.Rename.pathAsString
import de.ironjan.bib_helper.util.FileHelper
import org.backuity.clist.{Command, arg}

import scala.util.Success

object Download
extends Command
with Common
with PathArgument {

  def hasNoCompanion(p: Path) = {
    !hasCompanion(p)
  }

  private def hasCompanion(p: Path) = {
    val pAsString = p.toString
    val pdfPath = pAsString.substring(0, pAsString.lastIndexOf('.'))
    Files.exists(Paths.get(pdfPath))
  }

  def doDownloadStuffWithBibtex(bibTexFile: BibTexFile) = {
    val path = bibTexFile.path
    val pdfFileName = path.substring(path.lastIndexOf('/')+1, path.lastIndexOf('.'))

    if(bibTexFile.data.url.isEmpty){
     println(path + " has no url information.")
   }else {
     println(path)
     println(bibTexFile.basicInfo)
     println(bibTexFile.data.url.get)
      println(pdfFileName)
     println()
   }
  }

  def doDownloadStuff(p:Path) = {
BibTexParser.load(p) match {
  case Success(b: BibTexFile) => {
    doDownloadStuffWithBibtex(b)
  }
  case _=> {
    System.err.println(s"Could not load $p")
    System.exit(1)
  }
}
  }

  override def run(): Unit = {
    FileHelper.findBibFiles(path)
      .filter(hasNoCompanion)
      .foreach(doDownloadStuff)

  }
}
