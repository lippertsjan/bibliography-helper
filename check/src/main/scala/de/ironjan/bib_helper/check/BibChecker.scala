package de.ironjan.bib_helper.check

import java.nio.file.Path

import de.ironjan.bib_helper.bib_tex_lib.{BibTexFile, BibTexParser}

import scala.collection.mutable
import scala.util.Success

object BibChecker {

  def checkBibTextFile(b: BibTexFile) = {
    val data = b.data

    val errors = new mutable.ListBuffer[String]()
    if(data.author.forall(_.trim.isEmpty)){
  errors += "Invalid Author."
    }

    if(data.quoteKey.trim.isEmpty) {
      errors += "Invalid Quote key."
    }

    if(data.title.forall(_.trim.isEmpty)) {
      errors += "Invalid Title."
    }

    if(data.year.isEmpty) {
      errors += "Invalid Year."
    }


    if(!data.kvPairs.exists(kvp=>"url".equals(kvp.key))) {
      errors += "No url given."
    }

    errors.toSeq
  }

  def check(path:Path): Seq[String] = {
    BibTexParser.load(path) match {
      case Success(b: BibTexFile) => return checkBibTextFile(b)
      case _ => {
        System.err.println(s"Could not load $path")
        System.exit(1)
      }
    }
    Seq.empty[String]
  }

}
