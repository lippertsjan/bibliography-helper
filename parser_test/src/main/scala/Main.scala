import de.ironjan.bib_helper.bib_tex_lib.{BibTexData, BibTexParser}
import org.parboiled2.{ParseError, Parser}

import scala.util.{Failure, Success, Try}

object Main extends App {

  var s =
    """@inproceedings{Balme.2004,
      |  author       = {Balme, Lionel and Demeure, Alexandre and Barralon, Nicolas and Coutaz, Jo{\"e}lle and Calvary, Gaelle},
      |  year         = {2004},
      |  title        = {Cameleon-rt: A software architecture reference model for distributed, migratable, and plastic user interfaces},
      |  booktitle    = {European Symposium on Ambient Intelligence},
      |  pages        = {291--302},
      |  organization = Springer,
      |}""".stripMargin

  var t =
    """@article{Calvary.2003,
      |  author    = {{\"e}}
      |}""".stripMargin
  private val parser = new BibTexParser(s)
  parser.Content.run() match {
    case Success(_)             => println("Expression is valid")
    case Failure(e: ParseError) => println("Expression is not valid: " + parser.formatError(e))
    case Failure(e)             => println("Unexpected error during parsing run: " + e)
  }


}
