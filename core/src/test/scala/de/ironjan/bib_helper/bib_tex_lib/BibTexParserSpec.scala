package de.ironjan.bib_helper.bib_tex_lib

import java.io.File

import com.sun.net.httpserver.Authenticator
import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Failure, Success}


class BibTexParserSpec
  extends FlatSpec
    with Matchers{

  val resourcePath = "./src/test/resources/"
  val hars01Path = resourcePath + "hars01.pdf.bib"

  "Parsing a line" should "fail for empty string" in {
    runKVPairRule("") shouldBe a[Failure[_]]
  }

  it should "parse a valid author pair with quotes, spaces" in {
    runKVPairRule("author = \"Jan\"") shouldBe Success(KeyValuePair("author", "Jan"))
  }

  it should "fail when given no equal sign" in {
    runKVPairRule("author\"Jan\",") shouldBe a[Failure[_]]
  }

  "Parsing a quoted value" should "fail for empty string" in {
    runQuotedValueRule("") shouldBe a[Failure[_]]
  }

  it should "succeed when given a quoted value" in {
    runQuotedValueRule("\"test\"") shouldBe Success("test")
  }
  "Parsing a braced value" should "fail for empty string" in {
    runBracedValueRule("") shouldBe a[Failure[_]]
  }

  it should "succeed when given a braced value" in {
    runBracedValueRule("{test}") shouldBe Success("test")
  }

  "Parsing the bibtext body" should "fail for an empty string" in {
    runKVMapRule("") shouldBe a[Failure[_]]
  }

  it should "parse a single value correctly" in {
    runKVMapRule("author = \"Jan\",") shouldBe Success(Vector(KeyValuePair("author", "Jan")))
  }

  it should "parse a single value without spaces correctly" in {
    runKVMapRule("author=\"Jan\",") shouldBe Success(Vector(KeyValuePair("author", "Jan")))
  }

  it should "parse 2 values correctly" in {
    runKVMapRule("author = \"Jan\",\nyear = \"2018\"") shouldBe
      Success(
        Vector(
          KeyValuePair("author", "Jan"),
          KeyValuePair("year", "2018")))
  }

  it should "parse 2 values with dangling comma correctly" in {
    runKVMapRule("author = \"Jan\",\nyear = \"2018\",") shouldBe
      Success(
        Vector(
          KeyValuePair("author", "Jan"),
          KeyValuePair("year", "2018")))
  }
  it should "parse 3 values correctly" in {
    runKVMapRule("author = \"Jan\",\nyear = \"2018\",\n title = \"test\"") shouldBe
      Success(
        Vector(
          KeyValuePair("author", "Jan"),
          KeyValuePair("year", "2018"),
          KeyValuePair("title", "test")))
  }

  it should "parse 3 values with dangling comma correctly" in {
    runKVMapRule("author = \"Jan\",\nyear = \"2018\",\n title = \"test\",") shouldBe
      Success(
        Vector(
          KeyValuePair("author", "Jan"),
          KeyValuePair("year", "2018"),
          KeyValuePair("title", "test")))
  }

  "Type rule" should "capture article" in {
    new BibTexParser("@article").SourceType.run() shouldBe Success("article")
  }

  it should "capture book" in {
    new BibTexParser("@book").SourceType.run() shouldBe Success("book")
  }

  "Layout" should "do things" in {
    new BibTexParser("@article{quoteKey,\n  author=\"Jan\"}").Content.run() shouldBe
    Success(BibTexData("article", "quoteKey", Vector(KeyValuePair("author", "Jan"))))

  }
  "Content rule" should "parse a valid bibtex for 'An Effective Heuristic Algorithm for the Travelling-Salesman Problem'" in {
    val content = """@article{lin1973,
                    | author= {Shen Lin and Brian W. Kernighan},
                    | title = {An Effective Heuristic Algorithm for the Travelling-Salesman Problem},
                    | journal = {Operations Research},
                    | volume= {21},
                    | year= {1973},
                    | pages = {498--516}
                    | }""".stripMargin

    val result = new BibTexParser(content).Content.run()
    result shouldBe a[Success[_]]
  }

  it should "parse a valid bibtex for 'Working for free?'" in {
    val content = """@inproceedings{WorkingForFree,
                    |author={A. Hars and Shaosong Ou},
                    |booktitle={Proceedings of the 34th Annual Hawaii International Conference on System Sciences},
                    |title={Working for free? Motivations of participating in open source projects},
                    |year={2001},
                    |volume={},
                    |number={},
                    |pages={9 pp.-},
                    |abstract={The success of the Linux operating system has demonstrated the viability of an alternative form of software development: open source software, which challenges traditional assumptions about software markets. Understanding what drives open source developers to participate in open source projects is crucial for assessing the impact of open source software. The article identifies two broad types of motivations that account for their participation in open source projects. The first category includes internal factors such as intrinsic motivation and altruism, and the second category focuses on external rewards such as expected future returns and personal needs. The article also reports the results of a survey administered to open source programmers.},
                    |keywords={DP industry;Unix;human factors;professional aspects;programming;project management;public domain software;Linux operating system;altruism;expected future returns;external rewards;internal factors;intrinsic motivation;open source developers;open source programmers;open source projects;open source software;personal needs;software development;software markets;Business;Counting circuits;History;Licenses;Linux;Open source software;Operating systems;Programming profession;Software measurement;Sun},
                    |doi={10.1109/HICSS.2001.927045},
                    |ISSN={},
                    |month={Jan}}""".stripMargin

    val result = new BibTexParser(content).Content.run()
    result shouldBe a[Success[_]]
  }

  it should "parse a valid bibtex for 'The characteristics and motivations of library open source software developers: An empirical study'" in {
    val content = """@article{CHOI2015109,
                    |title = "The characteristics and motivations of library open source software developers: An empirical study",
                    |journal = "Library & Information Science Research",
                    |volume = "37",
                    |number = "2",
                    |pages = "109 - 117",
                    |year = "2015",
                    |issn = "0740-8188",
                    |doi = "https://doi.org/10.1016/j.lisr.2015.02.007",
                    |url = "http://www.sciencedirect.com/science/article/pii/S0740818815000341",
                    |author = "Namjoo Choi and Joseph A. Pruett",
                    |abstract = "Abstract Although there is an abundance of literature regarding the motivations of open source software (OSS) developers, researchers have not examined the specific motivations and characteristics of developers participating in library open source software (LOSS) projects. The characteristics and motivations of 126 LOSS developers associated with SourceForge, Foss4Lib, and Code4Lib are explored through an online survey. The questionnaire included items measuring select demographic attributes; scaled items measuring intrinsic, extrinsic, and internalized-extrinsic motivations; and open-ended questions. In comparison with the general OSS community, the results indicate that LOSS developers have high levels of intrinsic (i.e., altruism and fun) and internalized-extrinsic (i.e., learning and personal needs) motivations, higher diversity in gender, higher levels of formal education, previous library-related work experience, and a strong library ethos. Using this research, stakeholders can devise strategies to improve participation in LOSS projects."
                    |}""".stripMargin

    val result = new BibTexParser(content).Content.run()
    result shouldBe a[Success[_]]
  }
  it should "parse a valid bibtex for 'An Effective Heuristic Algorithm for the Travelling-Salesman Problem' with a Author.Year quote key" in {
    val content = """@article{Shen.1973,
                    | author= {Shen Lin and Brian W. Kernighan},
                    | title = {An Effective Heuristic Algorithm for the Travelling-Salesman Problem},
                    | journal = {Operations Research},
                    | volume= {21},
                    | year= {1973},
                    | pages = {498--516}
                    | }""".stripMargin

    val result = new BibTexParser(content).Content.run()
    result shouldBe a[Success[_]]
  }

  "load" should "should read file correctly" in {
    val bibTexData = BibTexParser.load(hars01Path)

    bibTexData shouldBe Success(BibTexFile((new File(hars01Path)).getAbsolutePath, BibTexData("inproceedings", "WorkingForFree",
      Seq(KeyValuePair("author", "A. Hars and Shaosong Ou"),
        KeyValuePair("year", "2001"),
        KeyValuePair("title", "Working for free? Motivations of participating in open source projects"),
        KeyValuePair("ISSN", ""),
        KeyValuePair("abstract", "The success of the Linux operating system has demonstrated the viability of an alternative form of software development: open source software, which challenges traditional assumptions about software markets. Understanding what drives open source developers to participate in open source projects is crucial for assessing the impact of open source software. The article identifies two broad types of motivations that account for their participation in open source projects. The first category includes internal factors such as intrinsic motivation and altruism, and the second category focuses on external rewards such as expected future returns and personal needs. The article also reports the results of a survey administered to open source programmers."),
        KeyValuePair("booktitle", "Proceedings of the 34th Annual Hawaii International Conference on System Sciences"),
        KeyValuePair("doi", "10.1109/HICSS.2001.927045"),
        KeyValuePair("keywords", "DP industry;Unix;human factors;professional aspects;programming;project management;public domain software;Linux operating system;altruism;expected future returns;external rewards;internal factors;intrinsic motivation;open source developers;open source programmers;open source projects;open source software;personal needs;software development;software markets;Business;Counting circuits;History;Licenses;Linux;Open source software;Operating systems;Programming profession;Software measurement;Sun"),
        KeyValuePair("month", "Jan"),
        KeyValuePair("number", ""),
        KeyValuePair("pages", "9 pp.-"),
        KeyValuePair("volume", "")))))
}

  "Quote Key" should "support author.year" in {
    val result = new BibTexParser("Hars.2001").QuoteKey.run()

    result shouldBe a[Success[_]]
  }

  private def runKVMapRule(value: String) =
    new BibTexParser(value).KeyValuePairs.run()

  private def runKVPairRule(value: String) =
    new BibTexParser(value).KeyValuePairRule.run()

  private def runQuotedValueRule(value: String) =
    new BibTexParser(value).QuotedValue.run()

  private def runBracedValueRule(value: String) =
    new BibTexParser(value).BracedValue.run()
}
