package de.ironjan.bib_helper.bib_tex_lib

import org.scalatest.{FlatSpec, Matchers}

import de.ironjan.bib_helper.bib_tex_lib.PrettyPrinter.BibTexDataPrettyPrinter

class BibTexPrettyPrinterSpec
  extends FlatSpec
    with Matchers {

  "An empty bib tex data" should "be printed correctly (@misc{quoteKey,})" in {
    BibTexData("misc", "quoteKey", Seq.empty[KeyValuePair]).pp shouldBe
      s"""|@misc{quoteKey,
          |}""".stripMargin
  }

  it should "be printed correctly with another ref type (@article{quoteKey,})" in {
    BibTexData("article", "quoteKey", Seq.empty[KeyValuePair]).pp shouldBe 
      s"""|@article{quoteKey,
          |}""".stripMargin
  }

  it should "be printed correctly with another quoteKey (@misc{quoteKey,})" in {
        BibTexData("misc", "quoteKey2", Seq.empty[KeyValuePair]).pp shouldBe 
          s"""|@misc{quoteKey2,
              |}""".stripMargin
  }

  "Bib tex data with only an author entry" should "be printed correctly" in {
    BibTexData("misc", "quoteKey", Seq(KeyValuePair("author", "Jan"))).pp shouldBe
      s"""|@misc{quoteKey,
          |  author = "Jan",
          |}""".stripMargin
  }

  "Bib tex data with 2/3 in {author, year, title}" should "be printed correctly for the case author and title" in {
    BibTexData("misc", "quoteKey", 
      Seq(
        KeyValuePair("author", "Jan"),
        KeyValuePair("title", "Some Spec")
      )).pp shouldBe
      s"""|@misc{quoteKey,
          |  author = "Jan",
          |  title  = "Some Spec",
          |}""".stripMargin
  }

  it should "be printed correctly for the case author and year" in {
    BibTexData("misc", "quoteKey", 
      Seq(
        KeyValuePair("author", "Jan"),
        KeyValuePair("year", "2018")
      )).pp shouldBe
      s"""|@misc{quoteKey,
          |  author = "Jan",
          |  year   = "2018",
          |}""".stripMargin
  }


  it should "be printed correctly for the case year and title" in {
    BibTexData("misc", "quoteKey", 
      Seq(
        KeyValuePair("title", "Test"),
        KeyValuePair("year", "2018")
      )).pp shouldBe
      s"""|@misc{quoteKey,
          |  year  = "2018",
          |  title = "Test",
          |}""".stripMargin
  }

  "Bib tex data with author, year, title" should "be printed correctly" in {
      BibTexData("misc", "quoteKey", 
      Seq(
        KeyValuePair("title", "Test"),
        KeyValuePair("author", "Jan"),
        KeyValuePair("year", "2018")
      )).pp shouldBe
      s"""|@misc{quoteKey,
          |  author = "Jan",
          |  year   = "2018",
          |  title  = "Test",
          |}""".stripMargin
  }

  "A big example" should "be printed correctly" in {
    BibTexData("inproceedings", "WorkingForFree", 
      Seq(
        KeyValuePair("author",    "A. Hars and Shaosong Ou"),
        KeyValuePair("booktitle", "Proceedings of the 34th Annual Hawaii International Conference on System Sciences"),
        KeyValuePair("title",     "Working for free? Motivations of participating in open source projects"),
        KeyValuePair("year",      "2001"),
        KeyValuePair("volume",    ""),
        KeyValuePair("number",    ""),
        KeyValuePair("pages",     "9 pp.-"),
        KeyValuePair("abstract",  "The success of the Linux operating system has demonstrated the viability of an alternative form of software development: open source software, which challenges traditional assumptions about software markets. Understanding what drives open source developers to participate in open source projects is crucial for assessing the impact of open source software. The article identifies two broad types of motivations that account for their participation in open source projects. The first category includes internal factors such as intrinsic motivation and altruism, and the second category focuses on external rewards such as expected future returns and personal needs. The article also reports the results of a survey administered to open source programmers."),
        KeyValuePair("keywords",  "DP industry;Unix;human factors;professional aspects;programming;project management;public domain software;Linux operating system;altruism;expected future returns;external rewards;internal factors;intrinsic motivation;open source developers;open source programmers;open source projects;open source software;personal needs;software development;software markets;Business;Counting circuits;History;Licenses;Linux;Open source software;Operating systems;Programming profession;Software measurement;Sun"),
        KeyValuePair("doi",       "10.1109/HICSS.2001.927045"),
        KeyValuePair("ISSN",      ""),
        KeyValuePair("month",     "Jan"))).pp shouldBe
    s"""|@inproceedings{WorkingForFree,
        |  author    = "A. Hars and Shaosong Ou",
        |  year      = "2001",
        |  title     = "Working for free? Motivations of participating in open source projects",
        |  ISSN      = "",
        |  abstract  = "The success of the Linux operating system has demonstrated the viability of an alternative form of software development: open source software, which challenges traditional assumptions about software markets. Understanding what drives open source developers to participate in open source projects is crucial for assessing the impact of open source software. The article identifies two broad types of motivations that account for their participation in open source projects. The first category includes internal factors such as intrinsic motivation and altruism, and the second category focuses on external rewards such as expected future returns and personal needs. The article also reports the results of a survey administered to open source programmers.",
        |  booktitle = "Proceedings of the 34th Annual Hawaii International Conference on System Sciences",
        |  doi       = "10.1109/HICSS.2001.927045",
        |  keywords  = "DP industry;Unix;human factors;professional aspects;programming;project management;public domain software;Linux operating system;altruism;expected future returns;external rewards;internal factors;intrinsic motivation;open source developers;open source programmers;open source projects;open source software;personal needs;software development;software markets;Business;Counting circuits;History;Licenses;Linux;Open source software;Operating systems;Programming profession;Software measurement;Sun",
        |  month     = "Jan",
        |  number    = "",
        |  pages     = "9 pp.-",
        |  volume    = "",
        |}""".stripMargin
  }
}