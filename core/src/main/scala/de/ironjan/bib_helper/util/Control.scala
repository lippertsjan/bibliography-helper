package de.ironjan.bib_helper.util

/**
  * FIXME documentation
  * From https://alvinalexander.com/scala/how-to-open-read-text-files-in-scala-cookbook-examples
  */
object Control {
  def using[A <: { def close(): Unit }, B](resource: A)(f: A => B): B =
  try {
    f(resource)
    } finally {
      resource.close()
    }
  }