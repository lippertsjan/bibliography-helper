package de.ironjan.bib_helper.util

import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{Files, Path}
import java.util.function.BiPredicate
import scala.collection.JavaConverters._

object FileHelper {
private  val getExtensionMatcher: String => BiPredicate[Path, BasicFileAttributes] = (ext)=>new BiPredicate[Path, BasicFileAttributes] {
    override def test(t: Path, u: BasicFileAttributes): Boolean = {
      t.toString.endsWith(ext)
    }
  }

  def findBibFiles(path:Path)= findFilesByExtension(path, ".bib")
  def findFilesByExtension(path: Path, ext:String) = {
    Files.find(path, 5, getExtensionMatcher(ext)).iterator().asScala.toSeq
  }
}
