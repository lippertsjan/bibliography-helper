package de.ironjan.bib_helper.util

import de.ironjan.bib_helper.conf.{Configuration, DefaultConfiguration}

trait Module {
  def run(conf: Configuration, args: Array[String]): Unit
  def run(args: Array[String]): Unit = run(DefaultConfiguration, args)
}