package de.ironjan.bib_helper.util

import java.io.File

case class Folders(input: File,
                   output: File)