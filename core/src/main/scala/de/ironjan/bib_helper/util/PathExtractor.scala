package de.ironjan.bib_helper.util

trait PathExtractor {
  def extractPath(args: Array[String], i: Int): String = {
    if (args.isEmpty) {
     throw new IllegalArgumentException("Argument list must not be empty")
   }

   args(i)
 }

 def extractPath(args: Array[String]): String = extractPath(args, 0)
}
