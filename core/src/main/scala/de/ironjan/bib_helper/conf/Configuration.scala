package de.ironjan.bib_helper.conf

trait Configuration {
  def DownloadDirectory: String
  def RenamedDirectory: String
  def SortedRootDirectory: String
}