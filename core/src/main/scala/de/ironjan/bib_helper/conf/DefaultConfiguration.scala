package de.ironjan.bib_helper.conf

object DefaultConfiguration
  extends Configuration {
  val DownloadDirectory: String = sys.env("HOME") + "/" + "papers" + "/" + "inbox"
  val RenamedDirectory: String = sys.env("HOME") + "/" + "papers" + "/" + "renamed"
  val SortedRootDirectory: String = sys.env("HOME") + "/" + "paper" + "/" + "sorted"
}