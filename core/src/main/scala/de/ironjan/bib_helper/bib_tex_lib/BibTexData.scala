package de.ironjan.bib_helper.bib_tex_lib

import com.github.dwickern.macros.NameOf._

import scala.util.{Failure, Success, Try}

/**
  * <p>Represents a bibfile's content.</p>
  *
  * @param refType  the reference type, e.g. inproceedings or article
  * @param quoteKey the key used to cite this reference
  * @param kvPairs  the bibfile's entries
  */
case class BibTexData(refType: String,
                      quoteKey: String,
                      kvPairs: Seq[KeyValuePair])
extends BibTextDataOps {
  lazy val basicInfo: String = Seq(author.getOrElse("NN"), " ",
    year.getOrElse("????"), ": ",
    title.getOrElse("Unknown title")).mkString("")

  def value(key: String) = dataMap.get(key)


  val firstAuthor = author.getOrElse("NN").split(",")(0)

  lazy val dataMap: Map[String, String] = kvPairs.map(p => (p.key, p.value)).toMap
  lazy val author: Option[String] = dataMap.get("author").map(_.split(",")(0))
  lazy val year: Option[Int] = Try(dataMap("year").toInt).toOption
  lazy val title: Option[String] = dataMap.get("title")
  lazy val url: Option[String] = dataMap.get("url")

  lazy val hasMandatoryFields: Boolean = author.nonEmpty && year.nonEmpty && title.nonEmpty

  def validate: Try[Boolean] = {
    val missingFields = Seq((author, nameOf(author)), (title, nameOf(title)), (year, nameOf(year)))
      .filter(_._1.isEmpty)
      .map(_._2)
      .mkString(", ")

    missingFields.length match {
      case 0 => Success(true)
      case _ => Failure(new BibTexDataValidationError(s"$missingFields missing."))
    }
  }
}

class BibTexDataValidationError(reason: String) extends Exception(reason)