package de.ironjan.bib_helper.bib_tex_lib

import java.io.PrintWriter

import de.ironjan.bib_helper.bib_tex_lib.PrettyPrinter.BibTexDataPrettyPrinter

import scala.util.{Failure, Success, Try}

object BibTexWriter {
  def save(bibTexFile: BibTexFile): Try[Unit] = {
    val printWriter = new PrintWriter(bibTexFile.path)

    try {
      printWriter.write(bibTexFile.data.pp)
      Success(Unit)
    }
    catch {
      case e: Exception => Failure(e)
    }
    finally {
      printWriter.close()
    }
  }
}
