package de.ironjan.bib_helper.bib_tex_lib

import scala.math.Ordering


object BibTexKeyOrdering
  extends Ordering[KeyValuePair] {

  def compare(x: KeyValuePair, y: KeyValuePair): Int =
    (x.key, y.key) match {
      case ("year", "author") => 1
      case ("title", "author") => 1
      case ("title", "year") => 1
      case ("url", "author") => 1
      case ("url", "year") => 1
      case ("url", "title") => 1
      case ("author", _) => -1
      case ("year", _) => -1
      case ("title", _) => -1
      case ("url", _) => -1
      case (_, "author") => 1
      case (_, "year") => 1
      case (_, "title") => 1
      case (_, "url") => 1
      case (a, b) => a.compare(b)
    }

}
