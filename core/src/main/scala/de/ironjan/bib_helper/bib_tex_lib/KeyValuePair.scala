package de.ironjan.bib_helper.bib_tex_lib

/**
  * <p>Represents an entry in a bibtex file, e.g. author = "Jan".</p>
  *
  * @param key   the entry's  key, e.g. author
  * @param value the entry's value, e.g. an author's name
  */
case class KeyValuePair(key: String, value: String)
