package de.ironjan.bib_helper.bib_tex_lib

object PrettyPrinter{
  implicit class BibTexDataPrettyPrinter(b: BibTexData) {
    def pp: String = {
      val sb = StringBuilder.newBuilder

      sb.append(s"@${b.refType}{${b.quoteKey},\n")
      computeEntryLines(b, sb).foreach(entry=>sb.append(entry).append(",\n"))
      sb.append("}")

      sb.mkString
    }
  }

  private def computeEntryLines(b: BibTexData, sb: StringBuilder): Seq[String] = {
    if(b.kvPairs.isEmpty) {
      return Seq[String]()
    }


    val longestKeyLength = b.kvPairs.map(_.key.length).max

    val entryToString = entryToStringFunctionBuilder(longestKeyLength)

    b.kvPairs
      .sorted(BibTexKeyOrdering)
      .map(entryToString)
  }

  implicit class MutableBibTexDataPrettyPrinter(b: MutableBibTexData) {
    def pp: String = {
      if (b.kvPairs.isEmpty)
        return s"""|@${b.refType}{${b.quoteKey},
                   |}""".stripMargin


      val longestKeyLength = b.kvPairs.map(_.key.length).max
      val paddedEntries =
        b.kvPairs
          .sorted(BibTexKeyOrdering)
          .map {case KeyValuePair(k,v) =>
            entryToString(k, v, longestKeyLength)
          }.mkString(",\n")

      s"""|@${b.refType}{${b.quoteKey},
          |$paddedEntries
          |}""".stripMargin

    }
  }

  /**
    * @param longestKeyLength length of the longest key in the bib file
    * @return a function that maps [[KeyValuePair]]s to string, based on the longest key length given
    */
  private def entryToStringFunctionBuilder(longestKeyLength:Int) = {
    keyValuePair: KeyValuePair => entryToString(keyValuePair.key, keyValuePair.value, longestKeyLength)
  }

  /**
    *
    * @param key the entry's key
    * @param value the entry's
    * @param longestKeyLength length of the longest key in the bib file
    * @return a line representing this entry, ready to be written into a file: '  key ..= "value",\n'
    */
  private def entryToString(key: String, value: String, longestKeyLength: Int): String = {
    val paddingSize = longestKeyLength - key.length
    val padding = " " * paddingSize
    val trimmedValue = value

    s"""  $key$padding = "$trimmedValue""""
  }
}