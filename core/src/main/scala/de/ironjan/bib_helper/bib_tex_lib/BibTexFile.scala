package de.ironjan.bib_helper.bib_tex_lib

case class BibTexFile(path: String,
                      data: BibTexData) {
  lazy val basicInfo: String = data.basicInfo

}