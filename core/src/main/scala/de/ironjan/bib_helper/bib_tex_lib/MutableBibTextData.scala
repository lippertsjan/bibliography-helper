package de.ironjan.bib_helper.bib_tex_lib

import com.github.dwickern.macros.NameOf._

import scala.collection._
import scala.util._

case class MutableBibTexData(refType: String,
                      quoteKey: String,
                      kvPairs: mutable.ListBuffer[KeyValuePair],
                             continueEditting: Boolean = true)
extends BibTextDataOps {
  lazy val dataMap: Map[String, String] = kvPairs.map(p => (p.key, p.value)).toMap
  lazy val author: Option[String] = dataMap.get("author").map(_.split(",")(0))
  lazy val year: Option[Int] = Try(dataMap("year").toInt).toOption
  lazy val title: Option[String] = dataMap.get("title")

  lazy val hasMandatoryFields: Boolean = author.nonEmpty && year.nonEmpty && title.nonEmpty

  def validate: Try[Boolean] = {
    val missingFields = Seq((author, nameOf(author)), (title, nameOf(title)), (year, nameOf(year)))
      .filter(_._1.isEmpty)
      .map(_._2)
      .mkString(", ")

    missingFields.length match {
      case 0 => Success(true)
      case _ => Failure(new BibTexDataValidationError(s"$missingFields missing."))
    }
  }
  val firstAuthor = author.getOrElse("NN").split(",")(0)

  def defaultQuoteKey = {
    val printableYear = year.getOrElse("Unknown")
    s"$firstAuthor.$printableYear"
  }
}
