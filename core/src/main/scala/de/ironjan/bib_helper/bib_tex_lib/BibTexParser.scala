package de.ironjan.bib_helper.bib_tex_lib

import java.io.File
import java.nio.file.Path

import org.parboiled2._
import org.parboiled2.CharPredicate._
import shapeless.HList

import scala.util._

/**
  * Parser to convert a bibtex file into an instance of [[BibTexData]].
  *
  * @param input the string to be parsed
  */
class BibTexParser(val input: ParserInput)
  extends Parser {

  def Content: Rule1[BibTexData] = rule {
    SourceType ~ '{' ~
      QuoteKey ~ "," ~ WS ~
      KeyValuePairs ~ WS ~ optional(",") ~ WS ~ '}' ~ WS ~> BibTexData
  }

  def SourceType: Rule1[String] = rule {
    '@' ~ capture(oneOrMore(AlphaNum))
  }

  def QuoteKey: Rule1[String] = rule {
    capture(oneOrMore(Printable--','))
  }

  def KeyValuePairs = rule {
    KeyValuePairRule + (optional(",") ~ WS)
  }

  def KeyValuePairRule: Rule1[KeyValuePair] = rule {
    Key ~ WS ~ '=' ~ WS ~ ValueRule ~> KeyValuePair
  }

  def ValueRule = rule {
    QuotedValue |
      BracedValue |
      capture(oneOrMore(Printable | AdditionalPrintables))
  }

  def QuotedValue: Rule1[String] = rule {
    '"' ~ capture(zeroOrMore(BracedContent | NonQuote)) ~ '"'
  }

  def BracedValue = rule {
    '{' ~ capture(zeroOrMore(BracedContent | QuotedValueContentChar )) ~ '}'
  }

  def BracedContent = rule {
    '{'~ oneOrMore(QuotedValueContentChar) ~'}'
  }

  def QuotedValueContentChar = rule {
    (Printable -- "}") | AdditionalPrintables
  }

  def Key: Rule1[String] = rule {
    capture(oneOrMore(AlphaNum ++ '-'))
  }


  def WS = rule {
    quiet(zeroOrMore(anyOf(" \n\r\t\f")))
  }

  def NonQuote = rule {
    (Printable -- '"') | AdditionalPrintables
  }

  def AdditionalPrintables = rule {
    Umlauts | '?' | '´' | '’'
  }

  def Umlauts = rule {
    "ä" | "ö" | "ü" | "Ä" | "Ö" | "Ü" | "ß"
  }


}

object BibTexParser {
  def load(path: Path): Try[BibTexFile] = load(path.toString)
  def load(path: String): Try[BibTexFile] = load(new File(path))

  def load(file: File): Try[BibTexFile] = {
    try {
      val source = scala.io.Source.fromFile(file)

      try {
        val fileContent = source.getLines.mkString("").replaceAll("\n", "")
        new BibTexParser(fileContent).Content.run().map { data =>
          BibTexFile(file.getAbsolutePath, data)
        }
      } catch {
        case e: Exception => Failure(e)
      } finally {
        source.close()
      }
    } catch {
      case e: Exception => Failure(e)
    }
  }
}

