# Bibliography Helper

Small tool, to help me organize literature.

## Modules

 * renamer <input dir> <output dir>
   * Renames pdfs and associated bib-files
   * Then copies them to output dir
 * bibedit <file.bib>
   * Displays the contents of file.bib and enables editing
   * Checks for (approximate) completeness

Planned:

 * associator <input dir>
   * Lists the bib files and their best pdf-match
   * Best match is decided by "roughly created at the same time"
 * categorizer <target root>
   * Lists existing categories
   * Can create new ones
 * env <opt: setup>
   * Checks if all folders configured are set up
   * The configuration (t.b.d.) is via file in $HOME/.bib_helper.conf
   * Format: to be done
 * sorter <input dir> <target root>
   * Copies the files to target root according to classification
   * E.g. "machine_learning/reinforment_learning"
 * finder <target root>
   * Searches target root for the given terms

# Architectural design

(Planned)

 * Different, independent modules as described above in a multi-project build
 * My own "bibtex"-library as another library (later deployed somewhere public I guess)
 * See below:

# Notes

Execute a single spec with `sbt ~test:testOnly *BibEditSpec`.
